#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/un.h>

// By default use IPv4. Compile with -DIPv6 to use IPv6 instead.
#ifndef IPv6
#define IPv4
#endif

int main(int argc, char *argv[]) {
  int udp_socket, port;
  struct sockaddr_in server_address;
  //struct sockaddr_in server_address = {AF_UNIX, "/tmp/socket.socket"};
  struct sockaddr_in client_address;
  char buffer[1024];
  ssize_t len, snd_len;
  socklen_t snda_len, rcva_len;
  int flags;
  
  if (argc != 2) { 
    fprintf(stderr, "Usage: %s [port_number]\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  port = atoi(argv[1]);

#ifdef IPv6
  server_address.sin_family = AF_INET6;
  if ((udp_socket = socket(PF_INET6, SOCK_DGRAM, 0)) == -1) {
      fprintf(stderr, "socket: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
  }
#else
  server_address.sin_family = AF_INET;
  if ((udp_socket = socket(PF_INET, SOCK_DGRAM, 0)) == -1) {
      fprintf(stderr, "socket: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
  } 
#endif
  printf("udp_socket = %d\n", udp_socket);
  //strcpy(server_address.sun_path, "/tmp/socket");
  server_address.sin_addr.s_addr = htonl(INADDR_ANY);
  server_address.sin_port = htons(port);

  if (bind(udp_socket, (struct sockaddr *) &server_address,
           (socklen_t) sizeof(server_address)) < 0) {
    fprintf(stderr, "bind: %s\n", strerror(errno));
    goto CLEANUP_SOCKET;
  }

  snda_len = (socklen_t) sizeof(client_address);
  for (;;) {
    do {
      rcva_len = (socklen_t) sizeof(client_address);
      flags = 0;
      len = recvfrom(udp_socket, buffer, sizeof(buffer), flags,
                     (struct sockaddr *) &client_address, &rcva_len);
      if (len == -1) {
        fprintf(stderr, "recvfrom: %s\n", strerror(errno));
        goto CLEANUP_SOCKET;
      } else {
        printf("read from socket %zd bytes %.*s\n", len, (int) len, buffer);
        printf("sending back to %u %d\n", client_address.sin_addr.s_addr,
                                           client_address.sin_port);
        snd_len = sendto(udp_socket, buffer, (size_t) len, 0,
                         (struct sockaddr *) &client_address, snda_len);
        if (snd_len != len) {
          fprintf(stderr, "sendto: %s\n", strerror(errno));
          goto CLEANUP_SOCKET;
        }
      }
    } while (len > 0);
  }

  if (close(udp_socket) != 0) {
    fprintf(stderr, "close: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }

  return 0;

CLEANUP_SOCKET:
  if (close(udp_socket) != 0)
    fprintf(stderr, "close: %s\n", strerror(errno));
  exit(EXIT_FAILURE);
}
