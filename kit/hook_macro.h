#define HOOK_FUNCTION(fname, sys_number, originall_call, type, new_fun) \
  static void fname(void) { \
    originall_call = (type) syscall_table[sys_number]; \
    x86_disable_write_protect(); \
    syscall_table[sys_number] = (unsigned long *)new_fun; \
    x86_enable_write_protect(); \
  }
