#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#include <openssl/hmac.h>
#include <openssl/evp.h>
#include <openssl/engine.h>

unsigned char* expected = (unsigned char*) "abcd1d87dca34f334786307d0da4fcbd";

int main(int argc, char* argv[]) {
  unsigned char* key, *result;
  if (argc != 4) {
    fprintf(stderr, "Usage: %s [key] [address] [port]\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  key = (unsigned char *) argv[1];
  while (1) {
    unsigned char result_hexstring[32];
    char* buff = NULL;
    size_t len = 0, n;
    n = getline(&buff, &len, stdin);
    if (n == -1)
      fprintf(stderr, "getline error: %s\n", strerror(errno));
    buff[n - 1] = '\0';
    printf("%s\n", buff);

    result = HMAC(EVP_md5(), key, strlen((char*)key), (unsigned char*)buff,
                  strlen(buff),
                  NULL, NULL);
    for (int i = 0; i < strlen((char *) result); i++) {
      sprintf(&(result_hexstring[i * 2]), "%02x", ((unsigned char *) result)[i]);
    }

    //printf("HMAC(%s) = %s\n", buff, result);
    if (strcmp((char *) result_hexstring, (char *) expected) == 0)
      printf("jupi\n");
    else
      printf("%d\n", strcmp((char *) result_hexstring, (char *) expected));

    for (int i = 0; i < strlen((char *)result_hexstring); ++i) {
      printf("%x:", result_hexstring[i]);
    }
    free(buff);
  }
  return 0;
}
